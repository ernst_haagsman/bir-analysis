import csv
from pylab import *

dataPoints = []

with open('DT_TrainingSet.csv', 'rb') as csvfile:
    setreader = csv.reader(csvfile, delimiter=",", quotechar='"')
    for row in setreader:
        try:
            dataPoints.append(int(row[1]))
        except ValueError:
            pass

boxplot(dataPoints[1:])
show()