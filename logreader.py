__author__ = 'EH'

import re
from datetime import datetime
from pandas import Series
from matplotlib import pyplot

date_re = re.compile(r'Jun (\d{2}), 2015 (\d{1,2}):(\d{2}):(\d{2}) [A|P]M')

datetimes = []

# read datetimes from the log file
with open('rapidminer_log.log', 'r') as logfile:
    for line in logfile:
        line_match = date_re.match(line)
        if line_match is not None:
            # convert 12 hour clock to 24 hour clock
            hour = int(line_match.group(2))
            if hour < 12:
                hour += 12
            else:
                hour -= 12
            time = datetime(2015, 6, int(line_match.group(1)), hour, int(line_match.group(3)), int(line_match.group(4)))
            datetimes.append(time)

print len(datetimes)
timeseries = Series(1, datetimes)
timeseries.resample('1Min', how='count').plot()
print timeseries.resample('1Min', how='count').head()
pyplot.show()